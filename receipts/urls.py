from django.urls import path
from receipts.views import (
    show_receipt_list,
    create_receipt,
    expense_category_list,
    account_list,
    create_category,
    create_account,
)


urlpatterns = [
    path("", show_receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
